# ITS Services Linux VM System Management PostgreSQL

This role helps to setup PostgreSQL from the original repository or one which is provided by the Satellite server

The following variables can be set:

 - postgresql_version: the version of postgresql to use. Default: "", Options: "10", "11", "12", "13", "14"
 - postgresql_repo: the type of repository it should use: Default: "original", Options: "original", "Satellite"
 - postgresql_satellite_repo: provide a reposiroy name which is configured on the Satellite server. Default: ""
 - postgresql_custom_user_name: PostgreSQL username to use. Default: "postgres"
 - postgresql_custom_group_name: PostgreSQL group to use. Default: "postgres"
 - postgresql_custom_user_id: custom user id to use. Default: ""
 - postgresql_custom_group_id: custom group id to use. Default: ""
 - postgresql_firewall_rule: define if the default port of postgresql should be open via firewall rule. Default: false
 - postgresql_pg_hba_config_choice: configuration of the pg_hba.config file. Default: "default", Options: "default", "custom"
 - postgresql_custom_pg_hba_config_local_lines: local lines for the config file. Default: "", Example:
   ```
   postgresql_custom_pg_hba_config_local_lines: |
        # "local" is for Unix domain socket connections only
        local   all             all                                     trust
        # IPv4 local connections:
        host    all             all             127.0.0.1/32            trust
        # IPv6 local connections:
        host    all             all             ::1/128                 trust
   ```
 - postgresql_custom_pg_hba_config_privilege_lines: privilege lines for the config file. Default: "", Example:
   ```
   postgresql_custom_pg_hba_config_privilege_lines: |
        host    [database name]        [username]      [ip]/[subnetmask]         trust
   ```

On the controlling host:

 - Ansible collection `community.general` (`ansible-galaxy collection install community.general`)

## Testing

Manualy tested on RHEL 7/8

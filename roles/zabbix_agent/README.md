# ITS Services Linux VM System Management Zabbix Agent

The role is a wrapper role for the community.zabbix collection that sets all the parameters for the ETH Zurich Zabbix Server
The roles makes use of the following variables which should be customized, i.e. in group_vars:

 - zabbix_agent_version: Define the version of your zabbix-agent. Default: "5.0", Options: "6.0", "5.4", "5.0", "4.0"
 - zabbix_repo: Choose the repo from which you want to install the zabbix-agent. For RHEL systems on satellite the option "other" will configure the zabbix repo from the satellite server. Options: zabbix, epel, other
 - zabbix_selinux: Set to False if you don't use SELinux. Default: True
 - zabbix_agent_satellite_repo_name: Name of the repo on the satellite server
 - zabbix_agent_package_state: Define if you want to have the newest package. Default: latest
 - zabbix_install_pip_packages: If you want to install pip packages for evaluation of IP addresses then set it to True. Default: False
 - zabbix_agent_hostname: If you would like to use a custom hostname. Default: "{{ ansible_fqdn }}"
 - zabbix_agent_firewall_enable: If you use iptables firewall set to True. Default: False
 - zabbix_agent_firewalld_enable: If you use firewalld set to True. Default: False
 - zabbix_agent_server: Choose your zabbix server.
 - zabbix_agent_serveractive: Choose your zabbix server active. Default: "{{ zabbix_agent_server }}"
 - zabbix_agent_hostnameitem: Chosse a custom hostnameitem
 - zabbix_agent_hostmetadata: Chosse a custom hostnameitem
 - zabbix_agent_hostmetadataitem: Chosse a custom hostnameite
 - zabbix_agent_listenport: Choose a zabbix server port. Default: 10050

Supported OS: Amazon, CentOS, RedHat, Fedora, OracleLinux, Scientific Linux, Ubuntu, Debian, OpenSuse, macOS, Windows

## Requirements

On the controlling host:

 - Ansible collection `community.zabbix` (`ansible-galaxy collection install community.zabbix`)
 - Python library `netaddr` (`pip install netaddr`)

## Testing

Debian is currently skipped, the zabbix-agent role fails the idempotency test.

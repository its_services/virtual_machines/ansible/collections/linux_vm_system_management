# Ansible Collection - its_services.linux_vm_system_management

Roles for system management, ongoing maintenance

 - `zabbix_agent`: wrapper for the official zabbix agent installation role
 - `postgresql`: role to install and configure postgresql